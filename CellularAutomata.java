import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.lang.Math;

public class CellularAutomata extends JFrame {
    
    private JPanel gridPanel, controlPanel;
    private DrawingPanel drawingPanel;
    private JButton pauseUnpauseButton, stepButton;
    private JComboBox speedBox;
    private JTextField cellCoordinates, playIndicator;
    private final int WINDOW_WIDTH = 1050;
    private final int WINDOW_HEIGHT = 700;
    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenu optionMenu;
    private JMenu sizeMenu;
    private JMenuItem clearItem, exitItem, randomizeItem;
    private JRadioButtonMenuItem conwayItem;
    private JRadioButtonMenuItem toroidalItem;
    private JRadioButtonMenuItem grid25x15item, grid50x30item, grid100x60item, grid200x120item;
    private JCheckBox visibleGridItem;
    protected Timer timer;

    protected int gridWidth = 50;
    protected int gridHeight = 30;
    protected int[][] grid = new int[120][200];
    protected boolean filling;

    private enum UpdateRule {
        CONWAY
    };

    private enum EdgeRule {
        TOROIDAL
    };

    protected UpdateRule updateRule = UpdateRule.CONWAY;
    protected EdgeRule edgeRule = EdgeRule.TOROIDAL;
    private final String[] speedLabels = { "0.5 steps/sec", "1.0 steps/sec", "1.5 steps/sec", "2.0 steps/sec",
            "2.5 steps/sec", "3.0 steps/sec", "5.0 steps/sec", "10.0 steps/sec", "15.0 steps/sec", "20.0 steps/sec" };
    private final double[] speedList = { 0.5, 1.0, 1.5, 2.0, 2.0, 3.0, 5.0, 10.0, 15.0, 20.0 };
    protected int delay = (int) (1000 / speedList[6]);
    protected boolean playing = false;
    protected int dx;
    protected int dy;
    protected boolean visibleGrid = true;

    public CellularAutomata() {
        super("Conway's game of life");
        setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        buildMenuBar();
        buildPanels();
        add(gridPanel, BorderLayout.CENTER);
        add(controlPanel, BorderLayout.SOUTH);
        gridClear();
        timer = new Timer(delay, new TimerListener());
        setVisible(true);
    }

    private void buildMenuBar() {
        menuBar = new JMenuBar();

        // File menu:
        clearItem = new JMenuItem("Clear");
        randomizeItem = new JMenuItem("Randomly Seed");
        exitItem = new JMenuItem("Exit");

        fileMenu = new JMenu("File");
        fileMenu.add(clearItem);
        clearItem.addActionListener(new ClearListener());
        fileMenu.add(randomizeItem);
        randomizeItem.addActionListener(new RandomizeListener());
        fileMenu.add(exitItem);
        exitItem.addActionListener(new ExitListener());

        visibleGridItem = new JCheckBox("Display Grid", true);
        visibleGridItem.addItemListener(new VisibleGridListener());
        conwayItem = new JRadioButtonMenuItem("Conway's Game of Life", true);
        conwayItem.addActionListener(new UpdateRuleListener());

        toroidalItem = new JRadioButtonMenuItem("Toroidal Edges", true);
        toroidalItem.addActionListener(new EdgeRuleListener());

        grid25x15item = new JRadioButtonMenuItem("25 x 15");
        grid25x15item.addActionListener(new SizeRuleListener());
        grid50x30item = new JRadioButtonMenuItem("50 x 30", true);
        grid50x30item.addActionListener(new SizeRuleListener());
        grid100x60item = new JRadioButtonMenuItem("100 x 60");
        grid100x60item.addActionListener(new SizeRuleListener());
        grid200x120item = new JRadioButtonMenuItem("200 x 120");
        grid200x120item.addActionListener(new SizeRuleListener());

        ButtonGroup ruleGroup = new ButtonGroup();
        ruleGroup.add(conwayItem);

        ButtonGroup edgeGroup = new ButtonGroup();
        edgeGroup.add(toroidalItem);

        ButtonGroup sizeGroup = new ButtonGroup();
        sizeGroup.add(grid25x15item);
        sizeGroup.add(grid50x30item);
        sizeGroup.add(grid100x60item);
        sizeGroup.add(grid200x120item);

        optionMenu = new JMenu("Options");
        optionMenu.add(visibleGridItem);
        optionMenu.addSeparator();
        optionMenu.add(conwayItem);
        optionMenu.addSeparator();
        optionMenu.add(toroidalItem);
        optionMenu.addSeparator();
        optionMenu.add(grid25x15item);
        optionMenu.add(grid50x30item);
        optionMenu.add(grid100x60item);
        optionMenu.add(grid200x120item);

        menuBar.add(fileMenu);
        menuBar.add(optionMenu);
        setJMenuBar(menuBar);
    }

    private void buildPanels() {
        gridPanel = new JPanel();
        drawingPanel = new DrawingPanel();
        drawingPanel.addMouseListener(new MouseEvents());
        drawingPanel.addMouseMotionListener(new MouseEvents());
        gridPanel.add(drawingPanel);

        controlPanel = new JPanel();
        speedBox = new JComboBox(speedLabels);
        speedBox.setSelectedIndex(6);
        speedBox.addActionListener(new SpeedBoxListener());
        pauseUnpauseButton = new JButton("Pause/Play");
        pauseUnpauseButton.addActionListener(new PauseUnpauseButtonListener());
        stepButton = new JButton("Iterate");
        stepButton.addActionListener(new StepButtonListener());
        cellCoordinates = new JTextField(7);
        cellCoordinates.setEditable(false);
        cellCoordinates.setText("Cell 0, 0");
        playIndicator = new JTextField(4);
        playIndicator.setEditable(false);
        playIndicator.setText("Paused");
        controlPanel.add(cellCoordinates);
        controlPanel.add(playIndicator);
        controlPanel.add(pauseUnpauseButton);
        controlPanel.add(stepButton);
        controlPanel.add(speedBox);
    }

    protected void gridClear() {
        for (int i = 0; i < gridHeight; i++) {
            for (int j = 0; j < gridWidth; j++)
                grid[i][j] = 0;
        }

        drawingPanel.repaint();
    }

    protected void randomize() {
        boolean looping;
        double density = 0;

        // Input validation:
        do {
            looping = false;
            String input = JOptionPane.showInputDialog(null,
                    "Enter the density of \"on\" " + "cells (as a decimal between 0 and 1).", "Select Density",
                    JOptionPane.QUESTION_MESSAGE);
            try {
                density = Double.parseDouble(input);
            } catch (NumberFormatException e) {
                JOptionPane.showMessageDialog(null, "That wasn't a decimal value. Please retry.", "Error",
                        JOptionPane.ERROR_MESSAGE);
                looping = true;
            } finally {
                if (density < 0 || density > 1) {
                    JOptionPane.showMessageDialog(null, "That wasn't a value between 0 and 1." + " Please retry.",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    looping = true;
                }
            }
        } while (looping);

        for (int i = 0; i < gridHeight; i++) {
            for (int j = 0; j < gridWidth; j++) {
                double number = Math.random();
                if (number <= density)
                    grid[i][j] = 1;
                else
                    grid[i][j] = 0;
            }
        }

        drawingPanel.repaint();
    }

    protected void update() {
        int[][] tempGrid = new int[gridHeight][gridWidth];
        for (int i = 0; i < gridHeight; i++) {
            for (int j = 0; j < gridWidth; j++)
                tempGrid[i][j] = 0;
        }
        for (int i = 0; i < gridHeight; i++) {
            for (int j = 0; j < gridWidth; j++) {
                if (grid[i][j] == 1)
                    addAdjacent(tempGrid, i, j);
            }
        }

        for (int i = 0; i < gridHeight; i++) {
            for (int j = 0; j < gridWidth; j++) {

                if (grid[i][j] == 1) {
                    if (tempGrid[i][j] < 2 || tempGrid[i][j] > 3)
                        grid[i][j] = 0;
                } else {
                    if (tempGrid[i][j] == 3)
                        grid[i][j] = 1;
                }
            }
        }

        drawingPanel.repaint();
    }

    private void addAdjacent(int[][] tempGrid, int i, int j) {
        for (int a = -1; a < 2; a++) {
            for (int b = -1; b < 2; b++) {
                int y = i + a;
                int x = j + b;

                if (y < 0)
                    y = gridHeight - 1;
                if (y > gridHeight - 1)
                    y = 0;
                if (x < 0)
                    x = gridWidth - 1;
                if (x > gridWidth - 1)
                    x = 0;
                if (a != 0 || b != 0)
                    tempGrid[y][x]++;
            }
        }
    }

    private class DrawingPanel extends JPanel {
        private final int WIDTH = 1000;
        private final int HEIGHT = 600;

        // Standard drawing panel methods:

        public DrawingPanel() {
            setBackground(Color.WHITE);
            setPreferredSize(new Dimension(WIDTH, HEIGHT));
        }

        public void paintComponent(Graphics g) {
            super.paintComponent(g);

            dx = (int) WIDTH / gridWidth;
            dy = (int) HEIGHT / gridHeight;

            for (int i = 0; i < gridHeight; i++) {
                for (int j = 0; j < gridWidth; j++) {
                    if (grid[i][j] == 1) {
                        g.setColor(Color.GREEN);
                        g.fillRect(dx * j + 1, dy * i + 1, dx, dy);
                    } else if (grid[i][j] == 2) {
                        g.setColor(Color.RED);
                        g.fillRect(dx * j + 1, dy * i + 1, dx, dy);
                    }
                }
            }

            if (visibleGrid) {
                g.setColor(Color.BLACK);
                for (int i = 0; i < gridWidth; i++)
                    g.drawLine(dx * i, 0, dx * i, HEIGHT);
                for (int i = 0; i < gridHeight; i++)
                    g.drawLine(0, dy * i, WIDTH, dy * i);
            }
        }

        public int getWidth() {
            return WIDTH;
        }

        public int getHeight() {
            return HEIGHT;
        }
    }

    private class ExitListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }

    private class ClearListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            gridClear();
        }
    }

    private class RandomizeListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            randomize();
        }
    }

    private class VisibleGridListener implements ItemListener {
        public void itemStateChanged(ItemEvent e) {
            if (visibleGridItem.isSelected())
                visibleGrid = true;
            else
                visibleGrid = false;
        }
    }

    private class UpdateRuleListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            updateRule = UpdateRule.CONWAY;
        }
    }

    private class EdgeRuleListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            edgeRule = EdgeRule.TOROIDAL;
        }
    }

    private class SizeRuleListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (grid25x15item.isSelected()) {
                gridWidth = 25;
                gridHeight = 15;
            } else if (grid50x30item.isSelected()) {
                gridWidth = 50;
                gridHeight = 30;
            } else if (grid100x60item.isSelected()) {
                gridWidth = 100;
                gridHeight = 60;
            } else if (grid200x120item.isSelected()) {
                gridWidth = 200;
                gridHeight = 120;
            } else // Unexpected selections default to 50 x 30.
            {
                gridWidth = 50;
                gridHeight = 30;
            }
            gridClear();
        }
    }

    private class PauseUnpauseButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (playing) {
                timer.stop();
                playing = false;
                playIndicator.setText("Paused");
            } else {
                timer.start();
                playing = true;
                playIndicator.setText("Playing");
            }
        }
    }

    private class StepButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            timer.stop();
            playIndicator.setText("Paused");
            update();
        }
    }

    private class SpeedBoxListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            delay = (int) (1000 / speedList[speedBox.getSelectedIndex()]);
            timer.setDelay(delay);
        }
    }

    private class MouseEvents implements MouseListener, MouseMotionListener {
        private int cellX;
        private int cellY;

        public void mouseDragged(MouseEvent e) {
            reposition(e);
            grid[cellY][cellX] = (filling) ? 1 : 0;
            reposition(e);
        }

        public void mouseMoved(MouseEvent e) {
            reposition(e);
        }

        public void mouseClicked(MouseEvent e) {
        }

        public void mouseEntered(MouseEvent e) {
        }

        public void mouseExited(MouseEvent e) {
        }

        public void mousePressed(MouseEvent e) {
            reposition(e);
            filling = (grid[cellY][cellX] == 1) ? false : true;
            grid[cellY][cellX] = (filling) ? 1 : 0;
            reposition(e);
        }

        public void mouseReleased(MouseEvent e) {
        }

        public void reposition(MouseEvent e) {
            cellX = (int) (1.0 * e.getX()) / dx;
            cellY = (int) (1.0 * e.getY()) / dy;

            if (cellX > gridWidth - 1)
                cellX = gridWidth - 1;
            if (cellX < 0)
                cellX = 0;
            if (cellY > gridHeight - 1)
                cellY = gridHeight - 1;
            if (cellY < 0)
                cellY = 0;

            cellCoordinates.setText("Cell " + cellX + ", " + cellY);
            drawingPanel.repaint();
        }
    }

    private class TimerListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            update();
        }
    }

    public static void main(String[] args) {
        new CellularAutomata();
    }
}
