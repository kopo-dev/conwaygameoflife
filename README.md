To "compile"

javac CellularAutomata.java
jar -cvmf CellularAutomata.mf CellularAutomata.jar *.class

To run
java -jar CellularAutomata.jar
